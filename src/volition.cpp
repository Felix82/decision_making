#include <iostream>
#include <stdio.h>
#include <vector>
#include <regex>

#include <time.h>
#include <sys/time.h>

using namespace std;

const string definitions("will_determined(X) :- c(X), m(X), #count{Y : m(Y), X != Y} > 0, #count{Y : m(Y), v(Y, X)} = 0. will_neglecting(X) :- c(X), m(X), #count{Y : m(Y), v(Y, X)} > 0. will_enforced(X) :- c(X), not m(X), #count{Y : m(Y), v(Y, X)} = 0. irrational(X) :- c(X), not m(X), #count{Y : m(Y), v(Y,X)} > 0. simply_volitional(X) :- c(X), m(X), #count{Y : m(Y), X != Y} = 0, #count{Y : v(Y, X)} = 0. simply_rational(X) :- c(X), m(X), #count{Y : m(Y), X != Y} = 0, #count{Y : v(Y, X)} > 0. v(X, Z) :- v(X, Y), v(Y, Z). #count{X : c(X) : d(X)} = 1.");

string call_asp(string &s) {
    string query("echo \""+s+definitions+"\" | clingo 0");
    string result("");
    char buf[BUFSIZ];
    FILE *ptr;

    //cout << query << endl;

    if ((ptr = popen(query.c_str(), "r")) != NULL)
    {
      while (fgets(buf, BUFSIZ, ptr) != NULL) {
         result += buf;
      }
      fclose(ptr);
    }
   return result;
}

int classification(string &s) {
    string result = call_asp(s);
    cout << result;
    return 0;
}

int retrieval(string &s, vector<string> &vas) {

    /* For timing uncomment 
    struct timeval start, end;
    double start_t, end_t, t_diff;
    gettimeofday(&start, NULL);*/
   string result = call_asp(s);

    /* For timing uncomment 
    gettimeofday(&end, NULL);
    start_t = start.tv_sec + double(start.tv_usec) / 1e6;
    end_t = end.tv_sec + double(end.tv_usec) / 1e6;
    t_diff = end_t - start_t;
    printf("Parsing Time: %.9f", t_diff);*/
   //cout << result << endl;
   int pos;
   smatch match;
   for(int i = 0; i < vas.size(); i++) {
       
       regex re(vas[i]+"\\(([0-9]*)\\)");
       regex_search(result, match, re);
       if(match.size() > 1) {
         cout << match[1] << endl;
         return 0;
       } else {
         //cout << "nothing found!";
       }
       
       /*
       int p = result.find(vas[i]);
       if(p != -1) {
       int p1 = result.find('(');
       int p2 = result.find(')');
       cout << result.substr(p1+1, p2-p1-1) << endl;
       return 0;
       }
       */
   }
   return 0;
}



int main(int argc, char* argv[]) {


   string problem("");
   string show("");
   string state("n");
   string cs("");
   vector<string> vas;
   bool classify = false;
   for(int i = 1; i < argc; i++) {
      if(*argv[i] == 'd') {
           state = "d";
           problem += "d(";
           for(int j = i+1; j < argc && *argv[j] != 'm'; j++) {
             problem += string(argv[j]);
             if(*argv[j+1] != 'm') {
		problem += ";";
             }
           }
           problem += ").";
      } else if(*argv[i] == 'm') {
           state = "m";
          problem += "m(";
           for(int j = i+1; j < argc && *argv[j] != 'v'; j++) {
             problem += string(argv[j]);
             if(*argv[j+1] != 'v') {
		problem += ";";
             }
           }
           problem += ").";
      } else if(*argv[i] == 'v') {
           state = "v";
          problem += "v(";
           for(int j = i+1; j < argc && *argv[j] != 'a'; j++) {
             problem += string(argv[j]);
             if(*argv[j+1] != 'a') {
		problem += ";";
             }
           }
           problem += ").";
      } else if(*argv[i] == 'a') {
           state = "a";
      } else {
        if(state.compare("a") == 0) {
	    show += "#show "+string(argv[i])+"/1. ";
            vas.push_back(string(argv[i]));
        }
        /*
        } else {
            if(state.compare("d") == 0) {
                cs += "c("+string(argv[i])+") | ";
            }
	    problem += state+"("+string(argv[i])+"). ";
        }*/
      }
   }
   

    

   if(classify) {
       classification(problem);
   } else {
       if(cs.compare("") != 0){
          cs = cs.substr(0, cs.size()-3)+".";
       }
       problem = problem + cs + show;
       retrieval(problem, vas);
   }


   return 0;
} 

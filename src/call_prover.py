from subprocess import Popen, PIPE
import re

"""
Constants: Configuration
"""
#PROVER_EXECUTABLE = ["./mace4", '-n', '2']
PROVER_EXECUTABLE = ["./prover9", '-x']
#PROVER_EXECUTABLE = ["clingo", '-n1', '--verbose=0 asp_version.lp']  
TPTP_CONVERTER = "./tptp_to_ladr"
THEORY_FILE = "volition_theory.tptp"
SUCCESS_MESSAGE = "THEOREM PROVED"
VOLITIONAL_ATTITUDE = ["IRRATIONAL", "SIMPLY_RATIONAL", "SIMPLY_WILL_CONFORM", "WILL_DETERMINED", "WILL_ENFORCED", "WILL_NEGLECTING"]

#
CHOICE = 'd0'

"""
Check if the provers output contains
a success message or not.
"""
def bool_sat(out):
    return SUCCESS_MESSAGE in out

def analyze_output(out, symbol):
    c = re.search("(function\("+symbol+", \[(.*)\]\))", out)
    i = int(c.groups()[1])
    for v in VOLITIONAL_ATTITUDE:
        c = re.search("("+v+"\(\_\), \[(.*)\]\))", out)
        if list(map(int, c.groups()[1].split(",")))[i] == 1:
            return v
    return None

"""
Call the prover and return its output.
PREPARE() MUST BE CALLED FIRST
"""
def call_prover(problem):
    call_prover = Popen(PROVER_EXECUTABLE, stdin=PIPE, stdout=PIPE)
    answer_prover = call_prover.communicate(problem)
    return answer_prover[0].decode()

"""
Convert from TPTP to LADR
"""
def call_converter(problem):
    call_converter = Popen([TPTP_CONVERTER], stdin=PIPE, stdout=PIPE)
    answer_converter = call_converter.communicate(problem.encode())
    return answer_converter[0]

"""
Load file that encodes the background theory 
of volition.
"""
def load_theory():
    with open(THEORY_FILE, 'r') as myfile:
        return myfile.read().replace('\n', '')


"""
Prepare situation-dependent 
knowledge to be added to the background
theory.
MUST BE CALLED BEFORE CALL_PROVER()
"""
def prepare():
    result = ""
    number = 60
    num_rat = int(number/2)
    d = [str(i) for i in range(0,number)]
    m = [str(i) for i in range(0,num_rat)]
    v = [[str(i), str(i+1)] for i in range(0,number-1)]

    PROVER_EXECUTABLE[1] = '-n '+str(len(d))

    cw_on_d = "fof(cw_on_d, axiom, ![X]:(D(X) => ("
    for e in d:
        result += "fof(input, axiom, D("+e+"))."
        cw_on_d += "X = "+e+" | "
    cw_on_d = cw_on_d[:-2] + ")))."
    result += cw_on_d
	
    for e in m:
        result += "fof(input, axiom, M("+e+"))."
    for e in set(d)-set(m):
        result += "fof(input, axiom, ~M("+e+"))."
    if CHOICE != None:
        result += "fof(input, axiom, C("+CHOICE+"))."
    for e in v:
        result += "fof(input, axiom, V("+e[0]+", "+e[1]+"))."
        
    return result

"""
Prepare what has to be proven.
"""
def query():
    #return "fof(con1, axiom, ~![X]:(C(X) => (IRRATIONAL(X) | SIMPLY_RATIONAL(X) | WILL_DETERMINED(X) | WILL_IGNORANT(X) | SIMPLY_WILL_CONFORM(X) | WILL_ENFORCED(X))))."
    # WILL_DETERMINED(X), WILL_IGNORANT(X), SIMPLY_WILL_CONFORM(X), SIMPLY_RATIONAL(X)
    #return "fof(con1, conjecture, 
    return ""
"""
Main Loop
"""
if __name__ == "__main__":
    background_theory = load_theory()
    #input_str = call_converter(background_theory + prepare() + query())
    input_str = call_converter(background_theory)
    #input_str = background_theory + prepare() + query()
    #print(input_str)
    r = call_prover(input_str)
    print(r)
    #print(analyze_output(r, CHOICE))
    #print(bool_sat(r))

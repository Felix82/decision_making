#include <map>
#include <algorithm>
#include <math.h>
#include "ros/ros.h"
#include "decision_making/OrderOptions.h"


bool levelwiseTallying(const std::vector<std::string> &scale,
		       const std::vector<std::string> &options,
		       const std::vector<decision_making::Reason> &reasons,
		       std::map<std::string, int>* option_map)
{

	// Inititalize the qualitative scale
	std::map<std::string, int> scale_map;
	for(int i = 0; i < scale.size(); i++)
	{
		scale_map.insert(std::pair<std::string, int>(scale[i], i+1));
	}

	// Determine total count of reasons
	int c_reasons = reasons.size();


	// Evaluate each option
	for(int i = 0; i < options.size(); i++)
	{
		(*option_map).insert(std::pair<std::string, int>(options[i], 0));
	}

	decision_making::Reason r;

	for(int i = 0; i < reasons.size(); i++)
	{
		r = reasons[i];

		for(int j = 0; j < r.applies_to.size(); j++)
		{
			if(r.polarity == "pro")
			{
				(*option_map)[r.applies_to[j]] += pow(c_reasons, scale_map[r.strength]);
			}
			else if(r.polarity == "con")
			{
				(*option_map)[r.applies_to[j]] -= pow(c_reasons, scale_map[r.strength]);
			}
		}
	}

	return true;
}

bool evaluate(decision_making::OrderOptions::Request &req,
	   decision_making::OrderOptions::Response &res)
{

	// Evaluation
	std::map<std::string, int> option_map;
	if(req.mode =="LT")
	{
		ROS_INFO("Call Levelwise Tallying");
		levelwiseTallying(req.scale, req.options, req.reasons, &option_map);
	}

	// Sorting
  	ROS_INFO("Sorting");
	std::vector<std::pair<int, std::string> > items;
	for(int i = 0; i < req.options.size(); i++)
	{
		items.push_back(std::pair<int, std::string>(option_map[req.options[i]], req.options[i]));
	}
	std::sort(items.begin(), items.end(), std::greater<std::pair<int, std::string> >());


	// Generate Response
  	ROS_INFO("Response Generation");
	int last_rank = 0;
	int last_rating = 0;
	int counter = 0;
	for(std::vector<std::pair<int, std::string> >::const_iterator it = items.begin(); it != items.end(); ++it)
	{
		decision_making::OptionRanked opt;
		opt.name = it->second;


		if(counter == 0)
		{
			opt.rank = 0; // Best
			last_rank = 0;
			last_rating = it->first;
			++counter;
		}
		else
		{
			if(last_rating != it->first)
			{
				++last_rank;
				last_rating = it->first;
			}
			opt.rank = last_rank;
		}


		res.options.push_back(opt);
	}


	ROS_INFO("Done.");

	return true;
}



int main(int argc, char **argv)
{
	const std::string NODE_NAME = "bipolar_evaluation";

	ros::init(argc, argv, NODE_NAME);
	ros::NodeHandle n;

	ros::ServiceServer service = n.advertiseService(NODE_NAME+"/evaluation_service", evaluate);
	ROS_INFO("Service Running");
	ros::spin();

	return 0;

}

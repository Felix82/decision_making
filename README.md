# README #

This is a simple ROS-Service for bipolar decision making implementing

*D. Dubois, H. Fargier and J. Bonnefon (2008) "On the Qualitative Comparison of Decisions Having Positive and Negative Features", Volume 32, pages 385-417.*
https://www.jair.org/papers/paper2520.html

Currently, it supports the Levelwise Tallying decision rule which has been applied to the social activity placement problem in 

*Lindner, F., Eschenbach, C. (2013) Affordance-Based Activity Placement in Human-Robot Shared Environments. In G. Herrmann, M. J. Pearson, A. Lenz, P. Bremner, A. Spiers, U. Leonards (eds.), Social Robotics, 94–103, Springer.*

More information will follow soon.